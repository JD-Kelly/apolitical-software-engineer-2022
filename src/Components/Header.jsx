import React from 'react';

const Header = () => {
  return (
    <header className='page-title'>
      <h1>Apolitcal Employee Details</h1>
    </header>
  )
}

export default Header;
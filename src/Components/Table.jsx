import React from 'react';


const Table = ({ nameList, removeName }) => {
  console.log('names:', nameList);

// A function to capitalize the first letter of name to ensure sorting alphabetically works
const capitalizeName = () => {
  return nameList.sort().map(name => {
    return name.charAt(0).toUpperCase() + name.substring(1);
  })
}
  const alphabtizeNames = capitalizeName(nameList)


  return (
    <div className="table-title">
      <h2>Employees</h2>
      <table className="table">
          <tr>
            <th>Employee Name</th>
            <th>Delete Item</th>
          </tr>
              {alphabtizeNames.map((name, index) => (
                <tr key={index}> 
                  <td>
                    {name}
                  </td>
                  <td>
                    <button onClick={() => removeName()}>Delete</button>     
                  </td> 
                </tr>     
            ))}  
      </table>
    </div>
  )
};

export default Table;

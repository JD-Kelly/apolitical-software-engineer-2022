import React, {useState} from "react";

const Form = ({ addName }) => {
  const [name, setName]=useState("")

  // regex to ensure form input only allows valid names
 const regex = /^(?![\s.]+$)[a-zA-Z\s.']*$/

  const handleSubmit = (e) => {
    e.preventDefault();
    if(name !== "") {
      console.log(`You submitted the name: ${name}`)
      addName(name)
      setName("")
    } else {
      alert("Please enter a name")
    }
  }

  const handleOnChange = (e) => {
    if(e.target.value.match(regex)) {
      setName(e.target.value)
    } else {
      alert('Please enter a valid name')
    }
  }
  
  return (
    <form onSubmit={handleSubmit}>
      <div className="form">
      <input
      className="input-field"
      type="text"
      value={ name }
      onChange={handleOnChange}
      placeholder="Enter employees name..."
      />
      </div>
      <div className="submit-button" >
        <input className="button" type="submit"/>
      </div>
    </form>
  )
}

export default Form;
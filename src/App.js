import React, { useState } from 'react';

import "./App.css";
import Table from './Components/Table';
import Header from './Components/Header';
import Form from './Components/Form';

const App = () => {
  const [nameList, setNameList] = useState([]);

  const addName = (name) => {
    setNameList((preVals) => [...preVals, name])
    console.log(nameList)
  }

  // removes the name at index
  const removeName = (index) => {
    const newNameList = [...nameList];
    newNameList.splice(index, 1);
    setNameList(newNameList);
  };

  return (
    <div>
      <Header />
      <Form addName={addName}/>
      <Table nameList={nameList} removeName={removeName}/>
    </div>
  );
}

export default App;
